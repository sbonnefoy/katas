#!/bin/bash

#Script to convert seconds given as input into seconds, minutes, hours, days, years.
#This could be quite optimized.

#!/bin/bash

function duration() {
  if [[ "$1" == "0" ]]; then
    echo "now"
    exit 0
  fi
  
  years=$(($1 / 31536000))
  days=$((($1-${years}*31536000) / 86400))
  hours=$((($1-${years}*31536000-${days}*86400) / 3600 ))
  minutes=$((($1-${years}*31536000-${days}*86400-${hours}*3600) / 60))
  seconds=$(($1 % 60))

  separator=" and "
  count="0"

  if [ "${seconds}" = "0" ]; then seconds=""
  elif [ "${seconds}" = "1" ]; then
          seconds=${seconds}" second"
          count=$(($count +1))
  elif [ "${seconds}" -ge "2" ]; then
    seconds=${seconds}" seconds"
    count=$(($count +1))
  fi
  
  #Checking the minutes
  if [[ "${count}" -eq "1" ]]; then separator=" and "
  elif [[ "${count}" -eq "0" ]]; then separator=""
  else
        separator=", "
  fi

  if [ "${minutes}" = "0" ]; then minutes=""
  elif [ "${minutes}" = "1" ]; then
          minutes=${minutes}" minute"${separator}
          count=$(($count +1))
  else
    minutes=${minutes}" minutes"${separator}
    count=$(($count +1))
  fi

  #Checking the hours
  if [[ "${count}" = 1 ]]; then separator=" and "
  elif [[ "${count}" = 0 ]]; then separator=""
  else
        separator=", "
  fi

  if [ "${hours}" = "0" ]; then hours=""
  elif [ "${hours}" = "1" ]; then
          hours=${hours}" hour"${separator}
          count=$(($count +1))
  else
    hours=${hours}" hours"${separator}
    count=$(($count +1))
  fi

  #Checking the days
  if [[ "${count}" = 0 ]]; then separator=""
  elif [[ "${count}" = 1 ]]; then separator=" and "
  else
        separator=", "
  fi

  if [ "${days}" = "0" ]; then days=""
  elif [ "${days}" = "1" ]; then
          days=${days}" day"${separator}
          count=$(($count +1))
  else
    days=${days}" days"${separator}
    count=$(($count +1))
  fi

  #Checking the years
  if [[ "${count}" = 0 ]]; then separator=""
  elif [[ "${count}" = 1 ]]; then separator=" and "
  else
        separator=", "
  fi

  if [ "${years}" = "0" ]; then years=""
  elif [ "${years}" = "1" ]; then years=${years}" year"${separator}; count=$(($count +1))
  else
    years=${years}" years"${separator}
    count=$(($count +1))
  fi
  echo ${years}${days}${hours}${minutes}${seconds}
  }
duration "$1"