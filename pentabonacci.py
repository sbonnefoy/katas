#The description of the problem can be found at :
#https://www.codewars.com/kata/pentabonacci/train/python

def count_odd_pentaFib(n):
    default = [0,1,1,2,4]
    list = []
    odd=[]
    for i in range (0,n+1):
        if i<5:
            list.append(default[i])
            if default[i]%2!=0 and default[i] not in odd:
                odd.append(default[i])
                odd_float=len(odd)
        else:
            list.append(list[i-1]+list[i-2]+list[i-3]+list[i-4]+list[i-5])
            if list[i]%2!=0:
               odd_float +=1 
    return odd_float 